FROM nginx:1.19.4

COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./build /var/www/html

EXPOSE 80
