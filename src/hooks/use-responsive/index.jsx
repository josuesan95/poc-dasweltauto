import { useState, useEffect } from 'react';
import { useMediaQuery } from 'react-responsive';

const useResponsive = () => {
  const [isClient, setIsClient] = useState(true);

  const responsive = useMediaQuery({
    maxWidth: 767,
  });

  useEffect(() => {
    if (typeof window !== 'undefined') setIsClient(true);
  }, []);

  return {
    responsive: isClient ? responsive : false,
  };
};

export default useResponsive;
