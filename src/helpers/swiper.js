// eslint-disable-next-line import/prefer-default-export
export function isTouchDevice() {
  return (('ontouchstart' in window)
     || (navigator.maxTouchPoints > 0)
     || (navigator.msMaxTouchPoints > 0));
}
