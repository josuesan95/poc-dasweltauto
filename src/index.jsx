import React from 'react';
import ReactDOM from 'react-dom';
import HomePage from './pages/Home';
import './index.scss';

// ReactDOM.render(<HomePage />, document.getElementById('root'));
ReactDOM.hydrate(<HomePage />, document.getElementById('root'));
