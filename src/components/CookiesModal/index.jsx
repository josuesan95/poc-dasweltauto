import PropTypes from 'prop-types';
import React, { useState } from 'react';
import {
  Button,
  Col,
  Modal,
  Row,
} from 'react-bootstrap';
import CookiesDetailModal from '../CookiesDetailModal';
import AnalyticsService from '../../services/AnalyticsService';
import './index.scss';

const CookiesModal = ({ initAnalitics }) => {
  const [show, setShow] = useState(true);
  const [showDetail, setShowDetail] = useState(false);

  const handleAcceptCookies = () => {
    AnalyticsService.storeCookies(['analytics', 'comfort', 'personalization']);
    initAnalitics();
    setShow(false);
  };

  const handleCookieDetail = () => {
    setShow(false);
    setShowDetail(true);
  };

  return (
    <>
      <Modal centered size="lg" show={show} onHide={() => { }} className="modal-cookies-form">
        <Modal.Header className="px-3 px-md-5">
          <p className="bold h5 mb-0">Política de Cookies</p>
        </Modal.Header>
        <Modal.Body className="px-3 px-md-5 pb-5 pt-4">
          <p>
            Te informamos que a través de este sitio web gestionado por SEAT, S.A. y
            Volkswagen Group España
            Distribución, S.A. utilizamos cookies técnicas, de preferencia, analíticas para analizar
            los hábitos de navegación en el sitio web, y de navegación para
            mostrarle contenido acorde
            con sus intereses. Para obtener más información lea nuestra&nbsp;
            <a href="https://www.dasweltauto.es/es/footer/politica_cookies.html" target="_blank" rel="noopener noreferrer">Política de Cookies. </a>
            Al pulsar en “Aceptar la Política de Cookies” acepta su uso.
          </p>
          <Row className="justify-content-end flex-1 align-content-end">
            <Col xs="12" md="4">
              <Button
                className="rounded-pill p-sm py-2 d-block w-100 das-btn transparent mx-auto mr-md-0"
                onClick={handleCookieDetail}
              >
                <span>Cambiar ajustes</span>
              </Button>
            </Col>
            <Col xs="12" md="6">
              <Button
                className="rounded-pill p-sm py-2 d-block w-100 das-btn mx-auto mr-md-0 ml-md-auto mt-3 mt-md-0"
                onClick={handleAcceptCookies}
              >
                <span>Aceptar la Política de Cookies</span>
              </Button>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
      {showDetail && (
        <CookiesDetailModal initAnalitics={initAnalitics} />
      )}
    </>
  );
};

CookiesModal.propTypes = {
  initAnalitics: PropTypes.func.isRequired,
};

export default CookiesModal;
