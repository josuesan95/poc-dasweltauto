/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useEffect, useRef, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useResponsive } from '../../hooks';
import ShareModal from '../ShareModal';
import AnalyticsService from '../../services/AnalyticsService';
import './index.scss';

const ShareSvg = () => (
  <svg xmlns="http://www.w3.org/2000/svg" id="svg-share" x="0px" y="0px" viewBox="30 -33 100 100"><path d="M103,30.2c-6.5,0-12.2,3.5-15.3,8.7L73.4,31c0.9-2.3,1.5-4.8,1.5-7.5c0-5-1.9-9.6-4.9-13.1L87.9-1.4 c2.7,3.1,6.7,5,11.1,5c8.2,0,14.8-6.6,14.8-14.8S107.1-26,98.9-26s-14.8,6.6-14.8,14.8c0,1.9,0.4,3.8,1.1,5.5L65.9,7 c-3.2-2.1-6.9-3.3-11-3.3C43.9,3.6,35,12.6,35,23.6s8.9,19.9,19.9,19.9c6.5,0,12.3-3.2,16-8l14.8,8.2c-0.4,1.4-0.6,2.9-0.6,4.4 c0,9.9,8,17.9,17.9,17.9s17.9-8,17.9-17.9S112.9,30.2,103,30.2z" /></svg>
);

const Banner = () => {
  const { responsive } = useResponsive();
  const [visibleVideo, setVisibleVideo] = useState(false);
  const [statusVideo, setStatusVideo] = useState(false);
  const videoParentRef = useRef();
  const spotParentRef = useRef();

  const [modal, setModal] = useState(null);

  const setLogAnalitycs = () => {
    AnalyticsService.setEvent(
      [
        AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
      ],
      AnalyticsService.categorysName.bannerPlay,
      'click',
      `${window.location.pathname} - Spot`,
    );
  };

  const playVideo = () => {
    setStatusVideo(true);
    document.getElementById('full-video').play();
  };

  const pauseVideo = () => {
    setStatusVideo(false);
    document.getElementById('full-video').pause();
  };

  const handleVideo = () => {
    setVisibleVideo(true);
    playVideo();
    setLogAnalitycs();
  };

  const controlVideo = () => {
    if (statusVideo) {
      pauseVideo();
    } else {
      playVideo();
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', () => {
      const elem = document.querySelector('#banner');
      const posTopView = window.scrollY;
      const posButView = posTopView + window.innerHeight;
      const elemTop = elem.offsetTop;
      const elemBottom = elemTop + elem.offsetHeight;
      const data = posButView - elemBottom;
      if (data > elemBottom) {
        return;
      }
      const opacity = 1 - (((data * 100) / elemBottom) / 100);
      document.querySelector('.hero-shadow').style.opacity = opacity;
    });
  }, []);

  const toggleShareModal = () => {
    setModal((prevState) => {
      if (prevState === 'share') {
        return null;
      }
      return 'share';
    });
  };

  const RenderModal = () => {
    if (modal === 'share') {
      return (
        <ShareModal show toggleShareModal={toggleShareModal} />
      );
    }

    return <></>;
  };

  return (
    <>
      <div className="banner-stop">
        <div className="banner-stop-box">
          <div className="banner-intro-sticky mb-5">
            <Container fluid className="mb-5" id="banner">
              <Row>
                <Col xs="12" className="p-0">
                  {visibleVideo && (
                    <>
                      {!statusVideo && (
                        <button className="btn-control-video" onClick={() => playVideo()} type="button" aria-label="Play video">
                          <div className="icon play" />
                        </button>
                      )}
                      {statusVideo && (
                        <button className="btn-control-video" onClick={() => pauseVideo()} type="button" aria-label="Pausar video">
                          <div className="icon pause" />
                        </button>
                      )}
                      <button className="btn-share" onClick={() => toggleShareModal()} type="button" aria-label="Abrir modal compartir">
                        <ShareSvg />
                      </button>
                    </>
                  )}
                  {responsive && (
                    <>
                      <div
                        className={`video-container spot ${visibleVideo ? 'hide' : ''}`}
                        onClick={() => handleVideo()}
                        ref={spotParentRef}
                        dangerouslySetInnerHTML={{
                          __html: `
                        <video
                          loop
                          muted
                          autoplay
                          playsinline
                          preload="auto"
                          class="w-100 pointer"
                        >
                          <source src="${process.env.REACT_APP_LANDING_URL}/assets/videos/loopcoches-mobile.mp4" type="video/mp4" />
                        </video>`,
                        }}
                      />
                      <div
                        className={`video-container fullv ${!visibleVideo ? 'hide' : ''}`}
                        onClick={() => controlVideo()}
                        ref={videoParentRef}
                        dangerouslySetInnerHTML={{
                          __html: `
                      <video
                        playsinline
                        preload="auto"
                        loop
                        class="w-100 pointer"
                        id="full-video"
                      >
                        <source src="${process.env.REACT_APP_LANDING_URL}/assets/videos/dwa_video-mobile.mp4" type="video/mp4" />
                      </video>`,
                        }}
                      />
                    </>
                  )}
                  {!responsive && (
                    <>
                      <div
                        className={`video-container spot ${visibleVideo ? 'hide' : ''}`}
                        onClick={() => handleVideo()}
                        ref={spotParentRef}
                        dangerouslySetInnerHTML={{
                          __html: `
                      <video
                        loop
                        muted
                        autoplay
                        playsinline
                        preload="auto"
                        class="w-100 pointer"
                      >
                        <source src="${process.env.REACT_APP_LANDING_URL}/assets/videos/loopcoches.mp4" type="video/mp4" />
                      </video>`,
                        }}
                      />
                      <div
                        className={`video-container fullv ${!visibleVideo ? 'hide' : ''}`}
                        onClick={() => controlVideo()}
                        ref={videoParentRef}
                        dangerouslySetInnerHTML={{
                          __html: `
                      <video
                        playsinline
                        loop
                        preload="auto"
                        class="w-100 pointer"
                        id="full-video"
                      >
                        <source src="${process.env.REACT_APP_LANDING_URL}/assets/videos/dwa_video.mp4" type="video/mp4" />
                      </video>`,
                        }}
                      />
                    </>
                  )}
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </div>
      <div className="hero-shadow will-change" />
      <RenderModal />
    </>
  );
};

export default Banner;
