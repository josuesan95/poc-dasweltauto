import React from 'react';
import { Button as Btn, Spinner } from 'react-bootstrap';
import PropTypes from 'prop-types';

const Button = ({
  children,
  variant = 'primary',
  type = 'button',
  size = '',
  loading = false,
  className = '',
  onClick = null,
}) => (
  <Btn
    className={className}
    variant={variant}
    size={size}
    type={type}
    disabled={loading}
    onClick={onClick}
  >
    {!loading && (
    <>
      {children}
    </>
    )}
    {loading && (
    <>
      <Spinner className="mx-1" as="span" animation="grow" size="sm" role="status" aria-hidden="true" />
      <Spinner className="mx-1" as="span" animation="grow" size="sm" role="status" aria-hidden="true" />
      <Spinner className="mx-1" as="span" animation="grow" size="sm" role="status" aria-hidden="true" />
    </>
    )}
  </Btn>
);

Button.defaultProps = {
  size: '',
  loading: false,
  className: '',
  onClick: null,
  type: 'button',
  variant: 'primary',
};

Button.propTypes = {
  variant: PropTypes.string,
  size: PropTypes.string,
  type: PropTypes.string,
  loading: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.any.isRequired,
  onClick: PropTypes.func,
};

export default Button;
