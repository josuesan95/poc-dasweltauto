import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import AnalyticsService from '../../services/AnalyticsService';

const HowWorks = () => {
  const setLogAnalitycs = () => {
    AnalyticsService.setEvent(
      [
        AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
        AnalyticsService.analyticsName.FLOODLIGHT_ANALYTICS,
        AnalyticsService.analyticsName.FACEBOOK_ANALYTICS,
        AnalyticsService.analyticsName.GOOGLE_TAG_MANAGER_ANALYTICS,
      ],
      AnalyticsService.categorysName.buttonReserve,
      'click',
      {
        google: `${window.location.pathname} - Reserva Online`,
        facebook: `${window.location.pathname} - Reserva Online`,
        floodlight: `${window.location.pathname} - Reserva Online`,
        tagManager: {
          event: 'verCochesMarca',
          eventAction: 'reservaAhora',
          eventCategory: window.dataLayer[0].productInfo.vehicleBrand,
          eventLabel: window.dataLayer[0].productInfo.vehicleModel,
        },
      },
    );
  };

  return (
    <Container className="mb-5">
      <Row className="justify-content-center align-items-center">
        <Col xs="12" md="6" data-aos="fade-right">
          <h2 className="subtitle bold mb-md-4">
            ¿Cómo
            <br />
            <span className="text-primary">funciona?</span>
          </h2>
          <p className="">
            Visita nuestro
            <span className="bold"> listado de vehículos, </span>
            elige el coche que más te guste,
            haz clic en el botón reserva y listo, nadie más podrá comprar ese coche.
          </p>
          <p>
            El importe de la reserva dependerá del precio del coche que elijas y
            podrás consultarlo justo antes de iniciar el proceso de reserva
          </p>
        </Col>
        <Col xs="12" md="6">
          <div
            dangerouslySetInnerHTML={{
              __html: `
              <video
                loop
                muted
                autoplay
                playsinline
                preload="auto"
                class="w-100 more-mobile minus-mt"
              >
              <source src="${process.env.REACT_APP_LANDING_URL}/assets/videos/moviles.mp4" type="video/mp4" />
              </video>`,
            }}
          />
          <a
            href="https://www.dasweltauto.es/esp/reserva-online"
            className="m-auto rounded-pill p-sm py-2 d-block w-100 das-btn"
            onClick={setLogAnalitycs}
          >
            <span>Reserva ahora</span>
          </a>
        </Col>
      </Row>
    </Container>
  );
};
export default HowWorks;
