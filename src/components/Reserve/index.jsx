import React, { useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import AnalyticsService from '../../services/AnalyticsService';
import './index.scss';

const Reserve = () => {
  const setLogAnalitycs = () => {
    AnalyticsService.setEvent(
      [
        AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
        AnalyticsService.analyticsName.FLOODLIGHT_ANALYTICS,
        AnalyticsService.analyticsName.FACEBOOK_ANALYTICS,
        AnalyticsService.analyticsName.GOOGLE_TAG_MANAGER_ANALYTICS,
      ],
      AnalyticsService.categorysName.buttonReserve,
      'click',
      {
        google: `${window.location.pathname} - Reserva Online`,
        facebook: `${window.location.pathname} - Reserva Online`,
        floodlight: `${window.location.pathname} - Reserva Online`,
        tagManager: {
          event: 'verCochesMarca',
          eventAction: 'reservaElTuyo',
          eventCategory: window.dataLayer[0].productInfo.vehicleBrand,
          eventLabel: window.dataLayer[0].productInfo.vehicleModel,
        },
      },
    );
  };
  const isDesktop = () => {
    const w = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    return w > 768;
  };

  useEffect(() => {
    let lap = 0;
    const listenerScroll = window.addEventListener('scroll', () => {
      const top = document.querySelector('#banner-reserve')?.getBoundingClientRect().top;
      const scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;
      if (isDesktop()) {
        if (top === 64 && lap === 0) {
          lap = scrollPosition;
        }
        if (top <= 64 && scrollPosition - lap > 100) {
          document.querySelector('.reserve-banner h3').style.opacity = 1;
        } else {
          document.querySelector('.reserve-banner h3').style.opacity = 0;
        }
        return 0;
      }
      if (top <= 300) {
        document.querySelector('.reserve-banner h3').style.opacity = 1;
      } else {
        document.querySelector('.reserve-banner h3').style.opacity = 0;
      }
      return () => {
        window.removeEventListener('scroll', listenerScroll);
      };
    });
  }, []);
  return (
    <>
      <div className="reserve-box">
        <div className="reserve-sticky">
          <Container fluid className="mb-5 reserve-banner h-100">
            <Row>
              <Col xs="12" className="p-0 h-100" data-aos="zoom-in">
                <div className="banner h-100">
                  <img id="banner-reserve" className="w-100" src={`${process.env.REACT_APP_LANDING_URL}/assets/images/reserva-banner.png`} alt="Reservar" />
                  <h3 className="text-banner bold text-white">Y una vez hayas reservado...</h3>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Container className="mb-5 reserve-text-down">
        <Row className="justify-content-center align-items-center">
          <Col xs="12" md="6" className="z-5" data-aos="fade-right">
            <h2 className="subtitle bold mb-md-4">
              ¿Qué hago
              <br />
              <span className="text-primary">luego?</span>
            </h2>
            <p className="text-cropped-reserve">
              Tu
              <span className="bold"> concesionario se pondrá en contacto contigo </span>
              para acordar día y hora de visita para acudir a sus instalaciones o, si lo prefieres,
              podrás agendar una video llamada para tu comodidad. ¡Así de fácil!
            </p>
            <p className="text-cropped-reserve">
              Además recuerda que tambien podrás realizar una
              <span className="bold"> prueba de conducción </span>
              y comprobar si el coche es justo lo que necesitas.
            </p>
          </Col>
          <Col xs="12" md="6">
            <div
              dangerouslySetInnerHTML={{
                __html: `
              <video
                loop
                muted
                autoplay
                playsinline
                preload="auto"
                class="w-100"
              >
              <source src="${process.env.REACT_APP_LANDING_URL}/assets/videos/carousel.mp4" type="video/mp4" />
              </video>`,
              }}
            />
            <a
              href="https://www.dasweltauto.es/esp/reserva-online"
              className="m-auto rounded-pill p-sm py-2 d-block w-100 das-btn"
              onClick={setLogAnalitycs}
            >
              <span>
                Reserva el tuyo
              </span>
            </a>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Reserve;
