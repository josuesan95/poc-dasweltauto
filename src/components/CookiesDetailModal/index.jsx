import PropTypes from 'prop-types';
import React, { forwardRef, useState } from 'react';
import {
  Accordion,
  Button,
  Col,
  Modal,
  Row,
  useAccordionToggle,
} from 'react-bootstrap';
import AnalyticsService from '../../services/AnalyticsService';
import './index.scss';

let Customflag = true;

const CustomToggle = forwardRef(({ eventKey }, ref) => {
  const decoratedOnClick = useAccordionToggle(eventKey);
  return (
    <Button
      ref={ref}
      type="button"
      className={
        `rounded-pill p-sm py-2 d-block w-100 das-btn btnMore ${Customflag ? '' : 'active'}`
      }
      onClick={(e) => {
        e.preventDefault();
        decoratedOnClick();
        Customflag = !Customflag;
      }}
    >
      <span>{Customflag ? 'Mostrar más' : 'Mostrar menos'}</span>
      <img
        className="arrow"
        src={`${process.env.REACT_APP_LANDING_URL}/assets/images/arrow-down.svg`}
        alt="arrow"
      />
    </Button>
  );
});
CustomToggle.displayName = 'CustomToggle';

const CookiesDetailModal = ({ initAnalitics }) => {
  const [show, setShow] = useState(true);
  const [cookiesOptions, setCookiesOptions] = useState([
    { id: 1, text: 'Cookies Técnicas', value: true },
    { id: 2, text: 'Cookies Estadísticas', value: false },
    { id: 3, text: 'Cookies de Preferencia', value: false },
    { id: 4, text: 'Cookies de Navegación', value: false },
  ]);

  const handleAcceptCookies = () => {
    AnalyticsService.storeCookies(['analytics', 'comfort', 'personalization']);
    initAnalitics();
    setShow(false);
  };

  const handleConfirmCookies = () => {
    if (cookiesOptions[2].value && !cookiesOptions[3].value) {
      AnalyticsService.storeCookies(['analytics', 'comfort']);
    } else if (cookiesOptions[3].value && !cookiesOptions[2].value) {
      AnalyticsService.storeCookies(['analytics', 'personalization']);
    } else if (cookiesOptions[3].value && cookiesOptions[2].value) {
      AnalyticsService.storeCookies(['analytics', 'comfort', 'personalization']);
    } else if (!cookiesOptions[3].value && !cookiesOptions[2].value && cookiesOptions[1].value) {
      AnalyticsService.storeCookies(['analytics']);
    } else {
      AnalyticsService.storeCookies([]);
    }
    initAnalitics();
    return setShow(false);
  };

  const handleCheckbox = (index) => {
    const newCookiesOptions = [...cookiesOptions];

    if (index === 2) {
      newCookiesOptions[1].value = true;
      newCookiesOptions[index].value = !newCookiesOptions[index].value;
    }
    if (index === 3) {
      newCookiesOptions[1].value = true;
      newCookiesOptions[index].value = !newCookiesOptions[index].value;
    }

    if (index === 1) {
      newCookiesOptions[index].value = !newCookiesOptions[index].value;
      if (!newCookiesOptions[index].value) {
        newCookiesOptions[1].value = false;
        newCookiesOptions[2].value = false;
        newCookiesOptions[3].value = false;
      }
    }

    setCookiesOptions(newCookiesOptions);
  };

  return (
    <Modal
      centered
      size="lg"
      show={show}
      onHide={() => { }}
      className="modal-cookies-form"
    >
      <Modal.Header className="px-3 px-md-5">
        <p className="bold h5 mb-0">Cambiar ajustes de la Política de Cookies</p>
      </Modal.Header>
      <Modal.Body className="px-4 px-md-5 pb-5 pt-4">
        <p>
          Desde este panel de configuración el usuario puedes configurar el uso
          de cookies.
        </p>
        <Row>
          <Col md="12">
            <div className="d-flex flex-column flex-md-row flex-wrap flex-md-nowrap mb-4 align-items-start">
              {cookiesOptions.map((elem, i) => (
                <div className="cookie-opt" key={elem.id}>
                  <label
                    htmlFor={`cookies-${elem.id}`}
                    className="d-flex
                    justify-content-center
                    align-items-center
                    container-radio-button
                    p-extra-sm
                    container-radio-button
                    "
                  >
                    <input
                      value={elem.value}
                      checked={elem.value}
                      className="permission-input"
                      id={`cookies-${elem.id}`}
                      type="checkbox"
                      name={`cookies-${elem.id}`}
                      disabled={i === 0}
                      onChange={() => handleCheckbox(i)}
                    />
                    <span className="checkmark" />
                    <span className="ml-1 mr-2 p-checkbox">{elem.text}</span>
                  </label>
                </div>
              ))}
            </div>
          </Col>
        </Row>
        <Accordion>
          <CustomToggle eventKey="1" />
          <Accordion.Collapse eventKey="1" className="mt-3">
            <div className="description-policy">
              <div className="content">
                <p className="bold p-md">Cookies Técnicas</p>
                <p className="p-md">
                  Son necesarias para permitir el funcionamiento de las
                  funciones básicas del sitio web, como ofrecer un acceso
                  seguro.
                </p>
                <p className="bold p-md">Cookies Estadísticas</p>
                <p className="p-md">
                  De forma anónima, recogemos datos sobre la forma de
                  interactuar de los usuarios en nuestro sitio web y así
                  elaborar informes de tendencias sin identificar usuarios
                  individuales. La finalidad es conocer el nivel de recurrencia
                  de nuestros visitantes y los contenidos que resultan más
                  interesantes para mejorar las áreas más visitadas y hacer que
                  el usuario encuentre más fácilmente lo que busca.
                </p>
                <p className="bold p-md">Cookies de Preferencia</p>
                <p className="p-md">
                  Son aquellas que permiten recordar información para que el
                  usuario acceda al servicio con determinadas características
                  que pueden diferenciar su experiencia de la de otros usuarios,
                  como, por ejemplo, el idioma, el número de resultados a
                  mostrar cuando el usuario realiza una búsqueda, el aspecto o
                  contenido del servicio en función del tipo de navegador a
                  través del cual el usuario accede al sservicio o de la región
                  desde la que accede al servicio, etc…
                </p>
                <p className="bold p-md">Cookies de Navegación</p>
                <p className="p-md">
                  Nos permiten mostrarle contenidos adecuados a sus gustos e
                  intereses, tanto en nuestra página web como en las de
                  terceros. El Usuario tiene la posibilidad de eliminar este
                  tipo de cookies antes de iniciar la navegación por otras
                  páginas del sitio web.
                </p>
              </div>
            </div>
          </Accordion.Collapse>
        </Accordion>
        <Row className="justify-content-end flex-1 align-content-end">
          <Col xs="12" md="5">
            <Button
              className="rounded-pill p-sm py-2 d-block w-100 das-btn transparent"
              onClick={handleConfirmCookies}
            >
              <span>Confirmar mi selección</span>
            </Button>
          </Col>
          <Col xs="12" md="4">
            <Button
              className="rounded-pill p-sm py-2 d-block w-100 das-btn ml-md-auto mt-3 mt-md-0"
              onClick={handleAcceptCookies}
            >
              <span>Seleccionar todo</span>
            </Button>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  );
};

CookiesDetailModal.propTypes = {
  initAnalitics: PropTypes.func.isRequired,
};

CustomToggle.propTypes = {
  eventKey: PropTypes.string.isRequired,
};

export default CookiesDetailModal;
