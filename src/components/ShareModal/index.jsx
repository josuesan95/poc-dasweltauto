import React, {
  useState, useCallback, useEffect,
} from 'react';
import PropTypes from 'prop-types';
import {
  Modal, Row, Col, Container,
} from 'react-bootstrap';
import AnalyticsService from '../../services/AnalyticsService';

import './index.scss';

const ShareModal = ({ show, toggleShareModal }) => {
  const [facebook, setFacebook] = useState({ url: '' });
  const [twitter, setTwitter] = useState({ url: '', text: '', user: '' });
  const [email, setEmail] = useState({ url: '', subject: '', body: '' });
  const [whatsapp, setWhatsapp] = useState({
    url: '',
    text: '',
    mobile: false,
  });
  const handleClose = () => toggleShareModal();

  const getShareInformation = useCallback(() => {
    const SHARE_URL = window.location.href;
    setFacebook({
      url: `${SHARE_URL}`,
    });
    setTwitter({
      url: `${SHARE_URL}`,
      text: 'Reserva tu coche de ocasión antes que nadie 100% online',
    });
    setEmail({
      subject: 'Das WeltAuto es la marca de Coches de Ocasión garantizados por el Grupo Volkswagen (SEAT, Skoda, Volkswagen y Volkswagen Vehículos Comerciales).',
      body: `${SHARE_URL} Reserva tu coche de ocasión antes que nadie 100% online`,
    });
    setWhatsapp((prevState) => ({
      ...prevState,
      url: `${SHARE_URL}`,
      text: 'Reserva tu coche de ocasión antes que nadie 100% online',
    }));
  }, []);

  const isMobile = useCallback(() => {
    if (
      navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i)
    ) {
      setWhatsapp((prevState) => ({
        ...prevState,
        mobile: true,
      }));
    }
  }, []);

  useEffect(() => {
    getShareInformation();
    isMobile();
  }, [getShareInformation, isMobile]);

  const handleShareSocialMedia = (socialMedia) => {
    AnalyticsService.setEvent(
      [
        AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
      ],
      AnalyticsService.categorysName.share,
      'click',
      socialMedia,
    );
  };

  return (
    <div>
      <Modal
        centered
        show={show}
        onHide={handleClose}
        className="modal-contact-form"
      >
        <Modal.Header
          className="p-0 bg-light d-flex align-items-center"
          closeButton
        >
          <Container>
            <Row className="py-2">
              <Col>
                <span className="p-lg">Compartir</span>
              </Col>
            </Row>
          </Container>
        </Modal.Header>
        <Modal.Body className="py-4 py-5">
          <Container>
            <Row className="justify-content-center">
              <Col xs="auto">
                {/*
                  Facebook (url)
                  */}
                <span
                  onClick={() => handleShareSocialMedia('facebook')}
                  role="button"
                  tabIndex={0}
                >
                  <a
                    title="Facebook"
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`https://www.facebook.com/sharer/sharer.php?u=${facebook.url}`}
                  >
                    <svg
                      className="icon-share"
                      id="svg-facebook"
                      x="0px"
                      y="0px"
                      viewBox="8 -8.7 56.7 56.7"
                    >
                      <path d="M48.4,13h-7.6V8c0-1.9,1.2-2.3,2.1-2.3c0.9,0,5.4,0,5.4,0v-8.3l-7.4,0c-8.2,0-10.1,6.2-10.1,10.1V13H26v8.5h4.8 c0,10.9,0,24.1,0,24.1h10c0,0,0-13.3,0-24.1h6.8L48.4,13z" />
                    </svg>
                  </a>
                </span>
              </Col>
              <Col xs="auto">
                {/*
                  Twitter (url, text, @mention)
                  */}
                <span
                  onClick={() => handleShareSocialMedia('twitter')}
                  role="button"
                  tabIndex={0}
                >
                  <a
                    title="Twitter"
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`https://twitter.com/intent/tweet?url=${twitter.url}&text=${encodeURI(twitter.text)}`}
                  >
                    <svg
                      className="icon-share"
                      id="svg-twitter"
                      x="0px"
                      y="0px"
                      viewBox="8 -8.7 56.7 56.7"
                    >
                      <path d="M60.8,6.4C59,7.2,57.1,7.7,55,8c2.1-1.2,3.7-3.2,4.4-5.6c-2,1.2-4.1,2-6.4,2.5c-1.8-2-4.5-3.2-7.4-3.2 c-5.6,0-10.1,4.5-10.1,10.1c0,0.8,0.1,1.6,0.3,2.3C27.4,13.6,20,9.6,15,3.5c-0.9,1.5-1.4,3.2-1.4,5.1c0,3.5,1.8,6.6,4.5,8.4 c-1.7-0.1-3.2-0.5-4.6-1.3c0,0,0,0.1,0,0.1c0,4.9,3.5,9,8.1,9.9c-0.8,0.2-1.7,0.4-2.7,0.4c-0.7,0-1.3-0.1-1.9-0.2c1.3,4,5,6.9,9.4,7 c-3.5,2.7-7.8,4.3-12.6,4.3c-0.8,0-1.6,0-2.4-0.1c4.5,2.9,9.8,4.5,15.5,4.5c18.6,0,28.8-15.4,28.8-28.8c0-0.4,0-0.9,0-1.3 C57.8,10.2,59.5,8.4,60.8,6.4z" />
                    </svg>
                  </a>
                </span>
              </Col>
              <Col xs="auto">
                {/*
                  Email (subject, body)
                  */}
                <span
                  onClick={() => handleShareSocialMedia('email')}
                  role="button"
                  tabIndex={0}
                >
                  <a
                    title="Correo"
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`mailto:?subject=${email.subject}&body=${email.body}`}
                  >
                    <svg
                      className="icon-share"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 29 19"
                    >
                      <path
                        d="M27.33 0H1.67C.89 0 .25.61.25 1.36v16.29c0 .75.64 1.36 1.42 1.36h25.65c.79 0 1.42-.61 1.42-1.36V1.36C28.75.61 28.11 0 27.33 0zm-.95 4.75l-9.03 6.57a4.74 4.74 0 01-5.7 0L2.62 4.75V2.38l9.97 7.28c1.13.84 2.67.84 3.8 0l9.97-7.28v2.37z"
                        fillRule="evenodd"
                        clipRule="evenodd"
                      />
                    </svg>
                  </a>
                </span>
              </Col>
              <Col xs="auto">
                {/*
                  Whatsapp
                  */}
                <span role="button" tabIndex={0}>
                  <a
                    title="Whatsapp"
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`https://api.whatsapp.com/send?text=${whatsapp.text} ${whatsapp.url}`}
                  >
                    <svg
                      id="Capa_1"
                      className="icon-share"
                      data-name="Capa 1"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 60 60"
                    >
                      <path
                        className="cls-1"
                        d="M43.77,35.77S37.67,32.85,37,33.92c-0.49.74-1.93,2.41-2.36,2.91A1.18,1.18,0,0,1,33,37a20.29,20.29,0,0,1-6-3.71,22.69,22.69,0,0,1-4.15-5.18c-0.73-1.27.8-1.45,2.19-4.09A1.37,1.37,0,0,0,25,22.73c-0.19-.38-1.68-4.05-2.31-5.52S21.44,15.94,21,15.94c-1.44-.13-2.49-0.11-3.42.86-4,4.43-3,9,.44,13.88,6.78,8.88,10.4,10.52,17,12.78a10.35,10.35,0,0,0,4.7.3c1.43-.23,4.42-1.8,5-3.57a6.18,6.18,0,0,0,.45-3.56A3.07,3.07,0,0,0,43.77,35.77Z"
                        transform="translate(0 0)"
                      />
                      <path
                        className="cls-1"
                        d="M51.26,8.72l0-.1C32.08-10,.26,3.52.25,29.73a29.58,29.58,0,0,0,4,14.86L0,60l15.84-4.13C35.6,66.55,60,52.37,60,29.75A29.48,29.48,0,0,0,51.26,8.72ZM17.48,50.93l-0.9-.54L7.2,52.83l2.51-9.11-0.6-.94C-1.2,26.37,10.65,4.92,30.18,4.92A24.83,24.83,0,0,1,55,29.67C55,48.75,34,60.67,17.48,50.93Z"
                        transform="translate(0 0)"
                      />
                    </svg>
                  </a>
                </span>
              </Col>
            </Row>
          </Container>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default ShareModal;

ShareModal.propTypes = {
  show: PropTypes.bool.isRequired,
  toggleShareModal: PropTypes.func.isRequired,
};
