/* eslint-disable react/jsx-props-no-spreading */
import PropTypes from 'prop-types';
import React, {
  useCallback,
  useEffect,
  useRef,
} from 'react';
import {
  Col,
  Container,
  Nav,
  Navbar,
  Row,
} from 'react-bootstrap';
import { SimulateClick } from '../../constants/SimulateClick';
import './index.scss';

const CustomToggle = React.forwardRef(({ onClick }, ref) => (
  <div className="d-flex align-items-center flex-row justify-content-center">
    <div className="spacer-row-ham" />
    <span
      className="custom-toogle"
      role="button"
      tabIndex={0}
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        document.querySelector('.navbar-shadown').classList.toggle('active');
        document.querySelector('.box-ham').classList.toggle('active');
        document.querySelector('#nav-icon1').classList.toggle('open');
        onClick(e);
      }}
    >
      <div className="box-ham">
        <div id="nav-icon1">
          <span />
          <span />
          <span />
        </div>
      </div>
    </span>
  </div>
));

CustomToggle.displayName = 'CustomToggle';

const Header = () => {
  const refContainer = useRef();
  const closeMenu = () => {
    if (document.querySelector('.box-ham').classList.contains('active')) {
      SimulateClick(document.querySelector('.custom-toogle'));
    }
  };
  const handleDocumentClick = useCallback((e) => {
    const container = refContainer.current;
    if (e.target !== container && !container?.contains(e.target)) {
      closeMenu();
    }
  }, []);

  useEffect(() => {
    document.querySelectorAll('.list-nav-items a').forEach((elem) => {
      if (elem) {
        elem.addEventListener('click', closeMenu);
      }
    });
    if (document.querySelector('.logo-header')) {
      document
        .querySelector('.logo-header')
        .addEventListener('click', closeMenu);
    }
    document.addEventListener('click', handleDocumentClick);
    return () => {
      document.querySelectorAll('.list-nav-items a').forEach((elem) => {
        if (elem) {
          elem.removeEventListener('click', closeMenu);
        }
      });
      if (document.querySelector('.logo-header')) {
        document
          .querySelector('.logo-header')
          .removeEventListener('click', closeMenu);
      }
      document.removeEventListener('click', handleDocumentClick);
    };
  }, [handleDocumentClick]);

  return (
    <>
      <div className="p-fixed" ref={refContainer}>
        <Navbar expand="xs" className="p-fixed p-0">
          <div className="navbar-shadown">
            <div className="bg-custom-desk" />
            <Container>
              <Navbar.Brand>
                <a className="logo-wrapper logo-header" title="Das WeltAuto" href="/">
                  <img
                    src={`${process.env.REACT_APP_LANDING_URL}/assets/images/logo-das-2.svg`}
                    alt="Das WeltAuto"
                  />
                </a>
              </Navbar.Brand>
              <Navbar.Toggle
                aria-controls="basic-navbar-nav"
                as={CustomToggle}
              />
              <Navbar.Collapse id="basic-navbar-nav">
                <Row className="d-desk d-none d-md-flex justify-content-center">
                  <Col md={4}>
                    <Nav className="mr-auto width-max-content list-nav-items">
                      <a
                        className="nav-link"
                        title="Modelos Volkswagen con reserva online"
                        href="https://www.dasweltauto.es/esp/coches-vw-reserva-online"
                      >
                        Reserva Volkswagen
                      </a>
                      <a
                        className="nav-link"
                        title="Modelos SEAT con reserva online"
                        href="https://www.dasweltauto.es/esp/coches-seat-reserva-online"
                      >
                        Reserva SEAT
                      </a>
                      <a
                        className="nav-link"
                        title="Modelos Volkswagen Comerciales con reserva online"
                        href="https://www.dasweltauto.es/esp/?condicion[flag_reserva_online]=1&condicion[marca]=Volkswagen&condicion[modelo]=Caravelle&ordpi=B"
                      >
                        Reserva Volkswagen Comerciales
                      </a>
                    </Nav>
                  </Col>
                  <Col md={4}>
                    <Nav className="mr-auto width-max-content list-nav-items">
                      <a
                        className="nav-link"
                        title="Sobre Das WeltAuto"
                        href="https://www.dasweltauto.es/es/que-es-das-weltauto.html"
                      >
                        Sobre Das WeltAuto
                      </a>
                      <a
                        className="nav-link"
                        title="Garantía"
                        href="https://www.dasweltauto.es/es/garantia.html"
                      >
                        Garantía
                      </a>
                      <a
                        className="nav-link"
                        title="Financiación"
                        href="https://www.dasweltauto.es/es/financiacion-dwa.html"
                      >
                        Financiación
                      </a>
                      <a
                        className="nav-link"
                        title="Blog"
                        href="https://www.dasweltauto.es/es/Blog.html"
                      >
                        Blog
                      </a>
                    </Nav>
                  </Col>
                </Row>
                <Nav className="mr-auto width-max-content d-mobile d-md-none list-nav-items">
                  <a
                    className="nav-link"
                    href="https://www.dasweltauto.es/esp/coches-vw-reserva-online"
                    title="Modelos Volkswagen con reserva online"
                  >
                    Reserva Volkswagen
                  </a>
                  <a
                    className="nav-link"
                    href="https://www.dasweltauto.es/esp/coches-seat-reserva-online"
                    title="Modelos SEAT con reserva online"
                  >
                    Reserva SEAT
                  </a>
                  <a
                    className="nav-link"
                    href="https://www.dasweltauto.es/esp/?condicion[flag_reserva_online]=1&condicion[marca]=Volkswagen&condicion[modelo]=Caravelle&ordpi=B"
                    title="Modelos Volkswagen Comerciales con reserva online"
                  >
                    Reserva Volkswagen Comerciales
                  </a>
                  <a
                    className="nav-link"
                    title="Sobre Das WeltAuto"
                    href="https://www.dasweltauto.es/es/que-es-das-weltauto.html"
                  >
                    Sobre Das WeltAuto
                  </a>
                  <a
                    className="nav-link"
                    title="Garantía"
                    href="https://www.dasweltauto.es/es/garantia.html"
                  >
                    Garantía
                  </a>
                  <a
                    className="nav-link"
                    title="Financiación"
                    href="https://www.dasweltauto.es/es/financiacion-dwa.html"
                  >
                    Financiación
                  </a>
                  <a
                    className="nav-link"
                    title="Blog"
                    href="https://www.dasweltauto.es/es/Blog.html"
                  >
                    Blog
                  </a>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </div>
        </Navbar>
      </div>
    </>
  );
};

CustomToggle.propTypes = {
  onClick: PropTypes.func.isRequired,
};
export default Header;
