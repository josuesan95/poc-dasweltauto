import React, { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import AnalyticsService from '../../services/AnalyticsService';
import './index.scss';

const InformationStop = () => {
  const [visible, setVisible] = useState(false);

  const setLogAnalitycs = () => {
    AnalyticsService.setEvent(
      [
        AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
        AnalyticsService.analyticsName.FLOODLIGHT_ANALYTICS,
        AnalyticsService.analyticsName.FACEBOOK_ANALYTICS,
        AnalyticsService.analyticsName.GOOGLE_TAG_MANAGER_ANALYTICS,
      ],
      AnalyticsService.categorysName.buttonReserve,
      'click',
      {
        google: `${window.location.pathname} - Reserva Online`,
        facebook: `${window.location.pathname} - Reserva Online`,
        floodlight: `${window.location.pathname} - Reserva Online`,
        tagManager: {
          event: 'verCochesMarca',
          eventAction: 'reservaElTuyo',
          eventCategory: window.dataLayer[0].productInfo.vehicleBrand,
          eventLabel: window.dataLayer[0].productInfo.vehicleModel,
        },
      },
    );
  };

  useEffect(() => {
    const spotElement = document.querySelector('.spot video');
    spotElement.addEventListener('loadeddata', () => {
      if (spotElement.readyState >= 3) {
        setVisible(true);
      }
    });
    setTimeout(() => {
      setVisible(true);
    }, 1000);
  }, [setVisible]);

  useEffect(() => {
    const navHeight = document.getElementsByTagName('nav')[0].offsetHeight;
    const informationTitle = document.getElementById('informationTitle');
    informationTitle.style.width = `${informationTitle.offsetWidth + 10}px`;
    const hideTitle = document.getElementById('hideTitle');
    hideTitle.style.height = `${informationTitle.offsetHeight}px`;
    hideTitle.style.width = `${informationTitle.offsetWidth}px`;

    const carModels = document.getElementById('car-models');
    const carsModelsHeight = carModels.offsetHeight;
    let carsModelsHeightPercent = 0.4;
    window.addEventListener('scroll', () => {
      if (informationTitle.classList.contains('position-fixed')) {
        if (hideTitle.getBoundingClientRect().top - navHeight < 0) {
          informationTitle.classList.add('position-fixed');
          hideTitle.classList.remove('d-none');
          hideTitle.classList.add('d-block');
        } else {
          informationTitle.classList.remove('position-fixed');
          hideTitle.classList.remove('d-block');
          hideTitle.classList.add('d-none');
        }
      } else if (informationTitle.getBoundingClientRect().top - navHeight < 0) {
        informationTitle.classList.add('position-fixed');
        hideTitle.classList.remove('d-none');
        hideTitle.classList.add('d-block');
      } else {
        informationTitle.classList.remove('position-fixed');
        hideTitle.classList.remove('d-block');
        hideTitle.classList.add('d-none');
      }
      if (window.innerWidth >= 768 && window.innerWidth <= 1024) {
        carsModelsHeightPercent = 0.01;
      }
      if (carModels.getBoundingClientRect().top
        < (carsModelsHeight * carsModelsHeightPercent) * -1) {
        informationTitle.classList.remove('position-fixed');
        hideTitle.classList.remove('d-block');
        hideTitle.classList.add('d-none');
      }
    });
  }, []);

  return (
    <div className={`information-stop ${visible ? '' : 'hidden'}`}>
      <div className="information-stop-box">
        <div className="intro-sticky pt-3 pt-md-0 pb-5 pb-md-0 mb-md-5">
          <Container>
            <Row className="justify-content-center bg-white">
              <Col xs="12">
                <div className="bg-white" id="informationTitle">
                  <h2 className="subtitle text-center bold mb-md-4">
                    Que no se te
                    {' '}
                    <span className="text-primary">adelanten</span>
                  </h2>
                </div>
              </Col>
            </Row>
            <div id="hideTitle" className="d-none" />
            <Row className="justify-content-center">
              <Col xs="12">
                <p className="text-center text-cropped">
                  Entrar en la web a buscar coche, encontrarlo,
                  pensar, ir a la concesión y que no esté. En
                  <span className="bold"> Das WeltAuto </span>
                  esto no tiene por qué pasar, ya que muchos
                  de nuestros coches cuentan con el nuevo servicio de
                  <span className="bold"> reserva online. ¡Reserva el tuyo desde casa antes que nadie!</span>
                </p>
              </Col>
              <Col xs="9" sm="6" md="3">
                <a
                  href="https://www.dasweltauto.es/esp/reserva-online"
                  className="m-auto rounded-pill p-sm py-2 d-block w-100 das-btn mt-md-4"
                  onClick={setLogAnalitycs}
                >
                  <span>Reserva el tuyo</span>
                </a>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    </div>
  );
};
export default InformationStop;
