import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Swiper from 'react-id-swiper';
import AnalyticsService from '../../services/AnalyticsService';
import './index.scss';

const coches = [
  {
    id: 1,
    name: 'Volkswagen',
    shortname: 'vw',
    img: `${process.env.REACT_APP_LANDING_URL}/assets/images/vw.png`,
    href: 'https://www.dasweltauto.es/esp/coches-vw-reserva-online',
  },
  {
    id: 2,
    name: 'SEAT',
    shortname: 'seat',
    img: `${process.env.REACT_APP_LANDING_URL}/assets/images/seat.png`,
    href: 'https://www.dasweltauto.es/esp/coches-seat-reserva-online',
  },
  {
    id: 3,
    name: 'Škoda',
    shortname: 'skoda',
    img: `${process.env.REACT_APP_LANDING_URL}/assets/images/skoda.png`,
    href: 'https://www.dasweltauto.es/esp/reserva-online',
  },
  {
    id: 4,
    name: 'Volkswagen Comerciales',
    shortname: 'lcv',
    img: `${process.env.REACT_APP_LANDING_URL}/assets/images/lcv.png`,
    href: 'https://www.dasweltauto.es/esp/?condicion[flag_reserva_online]=1&condicion[marca]=Volkswagen&condicion[modelo]=Caravelle&ordpi=B',
  },
];
const Carousel = () => {
  const params = {
    autoplay: {
      delay: 4000,
      disableOnInteraction: true,
    },
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: (index, className) => {
        const { name } = coches[index];
        return `<span class="${className}">${name}</span>`;
      },
    },
  };

  const setLogAnalitycs = (shortname, name) => {
    switch (shortname) {
      case 'vw':
        AnalyticsService.setEvent(
          [
            AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
            AnalyticsService.analyticsName.FACEBOOK_ANALYTICS,
            AnalyticsService.analyticsName.GOOGLE_TAG_MANAGER_ANALYTICS,
          ],
          AnalyticsService.categorysName.buttonReserveOnlineVW,
          'click',
          {
            google: `${window.location.pathname} - Reserva Online ${name}`,
            facebook: `${window.location.pathname} - Reserva Online ${name}`,
            tagManager: {
              event: 'verCochesMarca',
              eventAction: 'clicBodegon',
              eventCategory: name.toUpperCase(),
              eventLabel: name.toUpperCase(),
            },
          },
        );
        break;
      case 'seat':
        AnalyticsService.setEvent(
          [
            AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
            AnalyticsService.analyticsName.FACEBOOK_ANALYTICS,
            AnalyticsService.analyticsName.GOOGLE_TAG_MANAGER_ANALYTICS,
          ],
          AnalyticsService.categorysName.buttonReserveOnlineSeat,
          'click',
          {
            google: `${window.location.pathname} - Reserva Online ${name}`,
            facebook: `${window.location.pathname} - Reserva Online ${name}`,
            tagManager: {
              event: 'verCochesMarca',
              eventAction: 'clicBodegon',
              eventCategory: name.toUpperCase(),
              eventLabel: name.toUpperCase(),
            },
          },
        );
        break;
      case 'skoda':
        AnalyticsService.setEvent(
          [
            AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
            AnalyticsService.analyticsName.FACEBOOK_ANALYTICS,
            AnalyticsService.analyticsName.GOOGLE_TAG_MANAGER_ANALYTICS,
          ],
          AnalyticsService.categorysName.buttonReserveOnlineSkoda,
          'click',
          {
            google: `${window.location.pathname} - Reserva Online ${name}`,
            facebook: `${window.location.pathname} - Reserva Online ${name}`,
            tagManager: {
              event: 'verCochesMarca',
              eventAction: 'clicBodegon',
              eventCategory: name.toUpperCase(),
              eventLabel: name.toUpperCase(),
            },
          },
        );
        break;
      case 'lcv':
        AnalyticsService.setEvent(
          [
            AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
            AnalyticsService.analyticsName.FACEBOOK_ANALYTICS,
            AnalyticsService.analyticsName.GOOGLE_TAG_MANAGER_ANALYTICS,
          ],
          AnalyticsService.categorysName.buttonReserveOnlineLcv,
          'click',
          {
            google: `${window.location.pathname} - Reserva Online ${name}`,
            facebook: `${window.location.pathname} - Reserva Online ${name}`,
            tagManager: {
              event: 'verCochesMarca',
              eventAction: 'clicBodegon',
              eventCategory: name.toUpperCase(),
              eventLabel: name.toUpperCase(),
            },
          },
        );
        break;
      default:
        break;
    }
  };

  return (
    <Container className="mb-5" data-aos="zoom-in">
      <Row>
        <Col xs="12">
          <Swiper {...params}>
            {coches.map((coche) => (
              <div key={coche.id}>
                <a
                  href={coche.href}
                  onClick={() => setLogAnalitycs(coche.shortname, coche.name)}
                >
                  <img className="w-100" src={coche.img} alt={`Coche ${coche.name}`} />
                </a>
              </div>
            ))}
          </Swiper>
        </Col>
      </Row>
    </Container>
  );
};

export default Carousel;
