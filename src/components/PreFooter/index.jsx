import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

const PreFooter = () => (
  <Container className="mb-5">
    <Row className="justify-content-center align-items-center">
      <Col xs="12" className="mb-4" data-aos="fade-up">
        <h2 className="subtitle bold text-center">
          No busques.
          <span className="text-primary"> Reserva.</span>
        </h2>
      </Col>
    </Row>
    <Row className="justify-content-center align-items-center" data-aos="fade-up">
      <Col xs="9" md="10">
        <img className="w-100" src={`${process.env.REACT_APP_LANDING_URL}/assets/images/coches-bodegon.png`} alt="coches" />
      </Col>
      <Col xs="6" md="3">
        <img className="w-100" src={`${process.env.REACT_APP_LANDING_URL}/assets/images/marcas.png`} alt="marcas" />
      </Col>
    </Row>
  </Container>
);
export default PreFooter;
