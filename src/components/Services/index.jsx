import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import AnalyticsService from '../../services/AnalyticsService';
import './index.scss';

const Services = () => {
  const setLogAnalitycs = () => {
    AnalyticsService.setEvent(
      [
        AnalyticsService.analyticsName.GOOGLE_ANALYTICS,
        AnalyticsService.analyticsName.FACEBOOK_ANALYTICS,
        AnalyticsService.analyticsName.GOOGLE_TAG_MANAGER_ANALYTICS,
      ],
      AnalyticsService.categorysName.buttonKnowMore,
      'click',
      {
        google: `${window.location.pathname} - Saber más`,
        facebook: `${window.location.pathname} - Saber más`,
        tagManager: {
          event: 'infoSevicios',
          eventAction: 'masInfo',
          eventCategory: 'irServicios',
          eventLabel: 'https://www.dasweltauto.es/es/que-es-das-weltauto/servicios-exclusivos.html',
        },
      },
    );
  };
  return (
    <Container className="mb-5 services-container">
      <Row className="justify-content-center align-items-center">
        <Col xs="12" className="z-5" data-aos="fade-up">
          <h2 className="subtitle bold text-center mb-4">
            Además, en Das WeltAuto te ofrecemos estos
            <br />
            <span className="text-primary">servicios exclusivos:</span>
          </h2>
          <p className="text-center text-cropped-services">
            Todos nuestros coches están revisados y certificados
            <span className="bold"> con la garantía del Grupo Volkswagen </span>
            (Volkswagen, SEAT, Skoda y Volkswagen Vehículos comerciales).
            Además, te ofrecemos múltiples
            <span className="bold"> opciones de financiación </span>
            y la
            <span className="bold"> opción de compra </span>
            a cambio de tu vehículo actual.
          </p>
          <p className="text-center bold">¿A qué estás esperando?</p>
        </Col>
        <Col xs="12" data-aos="fade-up">
          <div
            className="video-badges vd-positioned"
            dangerouslySetInnerHTML={{
              __html: `
              <video
                loop
                muted
                autoplay
                playsinline
                preload="auto"
                class="w-100 more-mobile minus-mt"
              >
              <source src="${process.env.REACT_APP_LANDING_URL}/assets/videos/iconos-servicios.mp4" type="video/mp4" />
              </video>`,
            }}
          />
        </Col>
        <Col xs="12" md="3" className="postion-btn-vd">
          <a
            href="https://www.dasweltauto.es/es/que-es-das-weltauto/servicios-exclusivos.html"
            className="rounded-pill p-sm py-2 d-block w-100 das-btn hovered mx-auto"
            onClick={setLogAnalitycs}
          >
            <span>Saber más</span>
          </a>
        </Col>
      </Row>
    </Container>
  );
};

export default Services;
