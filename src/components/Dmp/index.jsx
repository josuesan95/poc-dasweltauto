import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import './index.scss';

const BASE_URL = process.env.REACT_APP_LANDING_URL;

const Dmp = () => (
  <Container className="mb-5">
    <Row className="justify-content-center align-items-center">
      <Col xs="12">
        <img
          className="mb-5 logo-dmp"
          src={`${BASE_URL}/assets/images/logo-dmp.png`}
          alt="dmp program"
          loading="lazy"
          decoding="async"
        />
      </Col>
      <Col xs="12">
        <h2 className="h2 bold mb-4 text-primary">
          Las 9 pautas que da FACEBOOK para crear mejores anuncios en videos
        </h2>
      </Col>
      <Col xs="12">
        <ol className="list-dmp">
          <li>
            <span className="bold">Adaptabilidad móvil:</span>
            {' '}
            Los dispositivos móviles tienen una gran
            penetración en la sociedad actual,
            y son el medio con mayor tiempo de navegación en el mundo. Por lo tanto,
            para crear vídeos exitosos es importante adaptarlos a la presentación
            móvil de la red social.
            Lo que quiere decir que deben ser creados
            {' '}
            <span className="bold">
              en formato vertical 4:5, o
              cuadrado en el caso de Instagram.
            </span>
          </li>
          <li>
            <span className="bold">Debe entenderse sin sonido:</span>
            {' '}
            Por supuesto el sonido es una parte fundamental a la hora de crear un vídeo,
            y puede cumplir un papel fundamental a la hora de conectar con los usuarios. Pero,
            debido a que
            {' '}
            <span className="bold">de manera automática los vídeos en Facebook se reproducen sin sonido</span>
            {' '}
            es
            importante crearlos para que puedan entenderse aunque estén silenciados.
          </li>
          <li>
            <span className="bold">Destacar el producto:</span>
            {' '}
            Como la intención del vídeo es vender o generar
            una reacción en el público, es de suma importancia
            resaltar el producto o mensaje que se desea vender o transmitir.
          </li>
          <li>
            <span className="bold">Mantener la idea centrada:</span>
            {' '}
            Comunicar un único mensaje es crucial para el éxito de un video ad,
            pues permite ofrecer una visión más centrada al espectador.
          </li>
          <li>
            <span className="bold">Resaltar la marca desde el inicio:</span>
            {' '}
            El flujo de las redes sociales suele ser muy rápido, por ello es
            importante destacar la marca al comienzo del vídeo en orden de atraer
            al público objetivo rápidamente y evitar que pase de largo.
          </li>
          <li>
            <span className="bold">Ser directo es lo mejor:</span>
            {' '}
            En este punto va de la mano con el cuarto, y enfatiza
            la necesidad de ofrecer un mensaje claro y concreto
            para que así los usuarios puedan captar fácilmente la idea detrás del vídeo.
            Cuanto más tarde en aparecer un mensaje en un vídeo, más probable es que
            los usuarios se lo pierdan. Facebook destaca la importancia de desligarse
            de la forma de narrar tradicional (introducción-nudo-desenlace): al contar
            con un menor tiempo de vídeo es crucial causar el mayor impacto posible.
            Por ello es preferible empezar con fuerza, al contrario que en los formatos
            tradicionales, cuyo comienzo es lento y va escalando.
          </li>
          <li>
            <span className="bold">Apuesta por el dinamismo:</span>
            {' '}
            Un vídeo que comience con cambios de escena o fondos y muestre
            un movimiento más dinámico en su contenido tiene más
            probabilidad de atraer más audiencia. Por lo que es importante manejar
            estas herramientas e incorporarlas al principio del contenido.
          </li>
          <li>
            <span className="bold">Duración:</span>
            {' '}
            Facebook destaca la importancia no solo de crear un contenido conciso en su mensaje,
            sino también
            {' '}
            <span className="bold">que no sean demasiado largos.</span>
            {' '}
            Tal y como explica la red social
            {' '}
            <span className="italic">
              «los vídeos
              de formato corto puede generar un mejor retorno de la inversión que el estándares de
              15 o 30 segundos. Se recomienda alinear la duración con el
              objetivo de la campaña – por ejemplo, un
              vídeo de formato largo ayuda a la construcción de marca y
              aumenta la conexión emocional, mientas que
              los vídeos más cortos son útiles para conseguir una determinada acción».
            </span>
          </li>
          <li>
            <span className="bold">Impacto visual:</span>
            Facebook resalta la importancia de crear un contenido de alto impacto
            visual, por medio de colores brillantes, zooms, características más detalladas, etc.
            Así el vídeo podrá atraer a muchas más personas captando su atención,
            incluso antes de que se interesen en el producto.
          </li>
        </ol>
      </Col>
      <Col xs="12" className="d-flex justify-content-center">
        <img
          className="footer-ig"
          src={`${BASE_URL}/assets/images/footer-ig.jpg`}
          alt="dmp program"
          loading="lazy"
          decoding="async"
        />
      </Col>
    </Row>
  </Container>
);

export default Dmp;
