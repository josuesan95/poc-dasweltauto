import PropTypes from 'prop-types';
import React from 'react';
import { Footer, Header } from '..';
// import AnalyticsService from '../../services/AnalyticsService';
// import CookiesModal from '../CookiesModal';

const Layout = ({ children, preFooter }) => (
  <>
    <Header />
    <div className="body-page">
      {children}
      <div style={{ height: !preFooter ? '1px' : '0' }} />
    </div>
    <Footer />
    {/* {!dwaPrivacy && (
      <CookiesModal initAnalitics={initAnalitics} />
    )} */}
  </>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  preFooter: PropTypes.bool.isRequired,
};

export default Layout;
