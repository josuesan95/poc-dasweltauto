/* eslint-disable no-param-reassign */
import React, { useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import './index.scss';

const CarModels = () => {
  const getRandomNumber = (min, max) => (
    (Math.floor(Math.random() * (max - min) + min)).toString()
  );

  const emojis = [
    {
      id: 1,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 2,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 3,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 4,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 5,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 6,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 7,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 8,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 9,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 10,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 11,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 12,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 13,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 14,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 15,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 16,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 17,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 18,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 19,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 20,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 21,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 22,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 23,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 24,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 25,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 26,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 27,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 28,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 29,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 30,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 31,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 32,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 33,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 34,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 35,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 36,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 37,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 38,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 39,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 40,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 41,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 42,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 43,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 44,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 45,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 46,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-sunglass-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 47,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 48,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-star-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
    {
      id: 49,
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/emoji-party-face.png`,
      offset: getRandomNumber(600, 1000),
      delay: getRandomNumber(0, 500),
    },
  ];

  const carModels = [
    {
      id: 1,
      name: 'coche',
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/flyer1.jpg`,
    },
    {
      id: 2,
      name: 'coche',
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/flyer2.jpg`,
    },
    {
      id: 3,
      name: 'coche',
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/flyer3.jpg`,
    },
    {
      id: 4,
      name: 'coche',
      img: `${process.env.REACT_APP_LANDING_URL}/assets/images/flyer4.jpg`,
    },
  ];

  useEffect(() => {
    let informationTitle = document.getElementById('informationTitle');
    let offsetTopCarModelsSticky = 30 + informationTitle.offsetHeight + document.getElementsByTagName('nav')[0].offsetHeight;
    document.getElementsByClassName('car-models-sticky')[0].style.top = `${offsetTopCarModelsSticky}px`;
    window.addEventListener('resize', () => {
      informationTitle = document.getElementById('informationTitle');
      offsetTopCarModelsSticky = 30 + informationTitle.offsetHeight + document.getElementsByTagName('nav')[0].offsetHeight;
      document.getElementsByClassName('car-models-sticky')[0].style.top = `${offsetTopCarModelsSticky}px`;
    });
  }, []);

  return (
    <Container className="py-3 py-md-0 mt-md-5 car-models car-models-box" id="car-models">
      <Row className="justify-content-center car-models-sticky">
        {carModels.map(({ id, name, img }) => (
          <Col xs="6" md="3" key={id}>
            <div className="content-card mb-4 mb-md-0">
              <img className="w-100" src={img} alt={name} />
              <div className="emoji-mask">
                {emojis.map((emoji) => (
                  <span
                    // eslint-disable-next-line react/no-array-index-key
                    key={`x-${emoji.id}`}
                    className="emoji"
                    data-index={emoji.id}
                    role="img"
                    aria-label="emoji"
                    data-aos="zoom-in"
                    data-aos-offset={emoji.offset}
                    data-aos-delay={emoji.delay}
                    data-aos-anchor="#car-models"
                  >
                    <img src={emoji.img} alt="Emoji icon" />
                  </span>
                ))}
              </div>
            </div>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default CarModels;
