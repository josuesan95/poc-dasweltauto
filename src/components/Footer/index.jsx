import React, { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import {
  Facebook,
  Instagram,
  Twitter,
  Youtube,
} from '../../constants/icons';
import './index.scss';

const Footer = () => {
  const [socials, setSocials] = useState({
    twitter: '',
    facebook: '',
    instagram: '',
    youtube: '',
  });

  useEffect(() => {
    setSocials({
      twitter: 'https://twitter.com/DasWeltAutoEsp',
      facebook: 'https://www.facebook.com/DasWeltAutoEsp',
      instagram: 'https://www.instagram.com/dasweltauto_esp/',
      youtube: 'https://www.youtube.com/c/DasWeltAutoEspa%C3%B1aES',
    });
  }, []);
  return (
    <>
      <footer>
        <Container fluid className="pb-4">
          <Row className="justify-content-center justify-content-md-between mb-4">
            <Col xs="12" md="3">
              <img className="d-block w-100 mx-auto mx-md-0" src={`${process.env.REACT_APP_LANDING_URL}/assets/images/logo-das.svg`} alt="Das WeltAuto" />
            </Col>
            <Col xs="12" md="3" className="mt-4 mt-md-0">
              <div className="rrss d-flex justify-content-center justify-content-md-end">
                <span role="button" tabIndex={0}>
                  <a href={socials.facebook} target="_blank" rel="noopener noreferrer" className="brand mr-2" title="Facebook">
                    <Facebook />
                  </a>
                </span>
                <span role="button" tabIndex={0}>
                  <a href={socials.instagram} target="_blank" rel="noopener noreferrer" className="brand mr-2" title="Instagram">
                    <Instagram />
                  </a>
                </span>
                <span role="button" tabIndex={0}>
                  <a href={socials.twitter} target="_blank" rel="noopener noreferrer" className="brand mr-2" title="Twitter">
                    <Twitter />
                  </a>
                </span>
                <span role="button" tabIndex={0}>
                  <a href={socials.youtube} target="_blank" rel="noopener noreferrer" className="brand" title="Youtube">
                    <Youtube />
                  </a>
                </span>
              </div>
            </Col>
          </Row>
          <Row>
            <Col xs="12">
              <div className="footer-second flex-column flex-md-row">
                <span className="copyright text-center text-md-left mb-3 mb-md-0">© Das WeltAuto. 2020</span>
                <ul className="mb-0 pl-0 pl-auto">
                  <li>
                    <a
                      href="https://www.dasweltauto.es/es/footer/aviso_legal.html"
                    >
                      Aviso Legal
                    </a>
                  </li>

                  <li>
                    <a
                      href="https://www.dasweltauto.es/es/footer/politica_privacidad.html"
                    >
                      Política Privacidad
                    </a>
                  </li>

                  <li>
                    <a
                      href="https://www.dasweltauto.es/es/footer/mapa-web.html"
                    >
                      Mapa Web
                    </a>
                  </li>

                  <li>
                    <a
                      href="https://www.dasweltauto.es/es/footer/condiciones-generales-de-reserva.html"
                      className="last"
                    >
                      Condiciones de Reserva
                    </a>
                  </li>
                </ul>
              </div>
            </Col>
          </Row>
        </Container>
      </footer>

    </>
  );
};
export default Footer;
