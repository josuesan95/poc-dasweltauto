import React, { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import {
  Facebook,
  Instagram,
  Twitter,
  Youtube,
} from '../../constants/icons';
import './index.scss';

const Footer = () => {
  const [socials, setSocials] = useState({
    twitter: '',
    facebook: '',
    instagram: '',
    youtube: '',
  });

  useEffect(() => {
    setSocials({
      twitter: 'https://twitter.com/DasWeltAutoEsp',
      facebook: 'https://www.facebook.com/DasWeltAutoEsp',
      instagram: 'https://www.instagram.com/dasweltauto_esp/',
      youtube: 'https://www.youtube.com/c/DasWeltAutoEspa%C3%B1aES',
    });
  }, []);
  return (
    <>
      <footer>
        <Container fluid className="pb-4">
          <Row className="justify-content-start justify-content-md-between mb-4">
            <Col xs="12" md="3">
              <img className="w-100" src="/assets/images/logo-das.svg" alt="Das WeltAuto" />
            </Col>
            <Col xs="12" md="3" className="d-none d-md-flex">
              <img className="d-block ml-auto w-100 invert" src="/assets/images/marcas.png" alt="Das WeltAuto" />
            </Col>
          </Row>
          <Row className="mb-4">
            <Col xs="12" md="3">
              <p className="p-sm text-center text-md-left bold">Nuestras Marcas</p>
              <ul className="list-footer">
                <li>
                  <a href="https://www.dasweltauto.es/esp/turismos-volkswagen-de-segunda-mano">Volkswagen</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/esp/seat-de-segunda-mano">SEAT</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/esp/skoda-de-segunda-mano">ŠKODA</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/esp/furgonetas-de-segunda-mano">Volkswagen vehículos Comerciales</a>
                </li>
              </ul>
            </Col>
            <Col xs="12" md="3">
              <p className="p-sm text-center text-md-left mt-3 mt-md-0 bold">Servicios</p>
              <ul className="list-footer">
                <li>
                  <a href="https://www.dasweltauto.es/es/reserva-online.html">Reserva online</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/es/que-es-das-weltauto/Servicios.html">Servicios Exclusivos Das WeltAuto</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/es/financiacion-dwa.html">Financiación</a>
                </li>
                <li>Cita Previa</li>
                <li>Visita virtual</li>
                <li>Entrega en casa</li>
                <li>Firma digital</li>
              </ul>
            </Col>
            <Col xs="12" md="3">
              <p className="p-sm text-center text-md-left mt-3 mt-md-0 bold">Das WeltAuto</p>
              <ul className="list-footer">
                <li>
                  <a href="https://www.dasweltauto.es/es/garantia.html">Garantia</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/esp/red-de-concesionarios">Concesionarios</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/es/financiacion-dwa.html">Financiación</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/es/Blog.html">Blog</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/esp/dudas-covid">Covid-19</a>
                </li>
                <li>
                  <a href="https://www.dasweltauto.es/es/que-es-das-weltauto/FAQS.html">FAQs</a>
                </li>
              </ul>
            </Col>
          </Row>
          <Row className="justify-content-center mb-4 mb-md-0">
            <Col xs="12">
              <p className="text-center p-sm">Redes sociales</p>
              <div className="rrss m-auto d-flex">
                <span role="button" tabIndex={0}>
                  <a href={socials.facebook} target="_blank" rel="noopener noreferrer" className="brand mr-2" title="Facebook">
                    <Facebook />
                  </a>
                </span>
                <span role="button" tabIndex={0}>
                  <a href={socials.instagram} target="_blank" rel="noopener noreferrer" className="brand mr-2" title="Instagram">
                    <Instagram />
                  </a>
                </span>
                <span role="button" tabIndex={0}>
                  <a href={socials.twitter} target="_blank" rel="noopener noreferrer" className="brand mr-2" title="Twitter">
                    <Twitter />
                  </a>
                </span>
                <span role="button" tabIndex={0}>
                  <a href={socials.youtube} target="_blank" rel="noopener noreferrer" className="brand" title="Youtube">
                    <Youtube />
                  </a>
                </span>
              </div>
            </Col>
          </Row>
          <Row className="justify-content-center d-md-none">
            <Col xs="12">
              <p className="p-sm text-center bold">Powered by</p>
              <img className="m-auto d-block w-100 invert" src="/assets/images/marcas.png" alt="Das WeltAuto" />
            </Col>
          </Row>
        </Container>
      </footer>
      <div className="footer-second flex-column flex-md-row">
        <span className="copyright text-center text-md-left mb-3 mb-md-0">© Das WeltAuto. 2020</span>
        <ul className="mb-0 pl-0 pl-auto">
          <li>
            <a
              href="https://www.dasweltauto.es/es/footer/aviso_legal.html"
            >
              Aviso Legal
            </a>
          </li>

          <li>
            <a
              href="https://www.dasweltauto.es/es/footer/politica_privacidad.html"
            >
              Política Privacidad
            </a>
          </li>

          <li>
            <a
              href="https://www.dasweltauto.es/es/footer/mapa-web.html"
            >
              Mapa Web
            </a>
          </li>

          <li>
            <a
              href="https://www.dasweltauto.es/es/footer/condiciones-generales-de-reserva.html"
              className="last"
            >
              Condiciones de Reserva
            </a>
          </li>
        </ul>
      </div>
    </>
  );
};
export default Footer;
