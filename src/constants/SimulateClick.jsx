/**
 * Simulate a click event.
 * @public
 * @param {Element} elem  the element to simulate a click on
 */

export const SimulateClick = (elem) => {
  const evt = new MouseEvent('click', {
    bubbles: true,
    cancelable: true,
    view: window,
  });
  // If cancelled, don't dispatch our event
  const canceled = !elem.dispatchEvent(evt);
  if (canceled) {
    return canceled;
  }
  return evt;
};

export default SimulateClick;
