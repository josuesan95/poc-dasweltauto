import Snippets from './Snippets';

const TagManager = {
  dataScript(dataLayer) {
    const script = document.createElement('script');
    script.innerHTML = dataLayer;
    return script;
  },
  gtm(args) {
    const snippets = Snippets.tags(args);

    const noScript = () => {
      const noscript = document.createElement('noscript');
      noscript.innerHTML = snippets.iframe;
      return noscript;
    };

    const script = () => {
      const newScript = document.createElement('script');
      newScript.innerHTML = snippets.script;
      return newScript;
    };

    const dataScript = this.dataScript(snippets.dataLayerVar);

    return {
      noScript,
      script,
      dataScript,
    };
  },
  initialize({
    gtmId, events = {}, dataLayer, dataLayerName = 'dataLayer', auth = '', preview = '',
  }) {
    const gtm = this.gtm({
      id: gtmId,
      events,
      dataLayer: dataLayer || undefined,
      dataLayerName,
      auth,
      preview,
    });
    if (dataLayer) document.head.insertBefore(gtm.dataScript, document.head.childNodes[0]);
    document.head.insertBefore(gtm.script(), document.head.childNodes[1]);
    document.body.insertBefore(gtm.noScript(), document.body.childNodes[1]);
  },
  dataLayer({ dataLayer, dataLayerName = 'dataLayer' }) {
    if (window[dataLayerName]) return window[dataLayerName].push(dataLayer);
    const snippets = Snippets.dataLayer(dataLayer, dataLayerName);
    const dataScript = this.dataScript(snippets);
    return document.head.insertBefore(dataScript, document.head.childNodes[0]);
  },
};

export default TagManager;
