/* eslint-disable no-console */

const warn = (s) => {
  console.warn('[react-gtm]', s);
};

export default warn;
