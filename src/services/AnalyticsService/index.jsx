/* eslint-disable quote-props */
/* eslint-disable no-unused-expressions */
/* eslint-disable camelcase */
import ReactGA from 'react-ga';
import FBPixel from 'react-facebook-pixel';
import TwitterConvTrkr from 'react-twitter-conversion-tracker';
import GoogleTagManager from './GoogleTagManager';

const googleTagManagerAnalytics = {
  init: (pixel) => (
    GoogleTagManager.initialize({
      gtmId: pixel,
      dataLayer: JSON.parse(process.env.REACT_APP_DATALAYER) || {},
    })
  ),
  send: ({
    event, eventAction, eventCategory, eventLabel,
  }) => {
    window.dataLayer.push({
      event,
      event_ction: eventAction,
      event_category: eventCategory,
      event_abel: eventLabel,
    });
  },
};

const googleAnalytics = {
  init: (pixeles) => {
    ReactGA.initialize(
      [
        {
          trackingId: pixeles,
          gaOptions: { name: 'dwa' },
        },
      ],
      { debug: false, alwaysSendToDefaultTracker: false },
    );
  },

  logPage: (page) => {
    ReactGA.set({ page }, ['dwa']);
    ReactGA.pageview(page, ['dwa']);
  },

  send: (category, action, label) => {
    ReactGA.event(
      {
        category,
        action,
        label,
      },
      ['dwa'],
    );
  },
};

const facebookAnalytics = {
  init: (pixel) => {
    const advancedMatching = {};
    const options = {
      autoConfig: true,
      debug: false,
    };
    FBPixel.init(pixel, advancedMatching, options);
  },
  logPage: () => {
    FBPixel.pageView();
  },
  send: (event, data) => {
    FBPixel.fbq('track', event, { content_name: data });
  },
};

const twitterAnalytics = {
  init: (id) => {
    TwitterConvTrkr.init(id);
  },
  logPage: () => {
    TwitterConvTrkr.pageView();
  },
};

const floodlightAnalytics = {
  init: () => new Promise((accept) => {
    const existingScript = document.getElementById('gtagLaunch');
    if (!existingScript) {
      const scriptFlood = document.createElement('script');
      scriptFlood.src = 'https://www.googletagmanager.com/gtag/js?id=DC-6699907';
      scriptFlood.id = 'gtagLaunch';
      document.body.appendChild(scriptFlood);
      scriptFlood.onload = () => {
        const gaUserId = document.cookie.match(/_ga=(.+?);/)[1].split('.').slice(-2).join('.');
        window.dataLayer[0].pageInfo.sessionId = gaUserId;
        window.dataLayer[0].pageInfo.allow_custom_scripts = true;

        window.dataLayer = window.dataLayer || [];
        window.gtag = function gtag() {
          // eslint-disable-next-line prefer-rest-params
          window.dataLayer.push(arguments);
        };
        window.gtag('js', new Date());
        window.gtag('config', 'DC-6699907');

        window.gtag('event', 'conversion', {
          allow_custom_scripts: true,
          send_to: 'DC-6699907/es_dwa01/es_dw00g+standard',
        });
        accept();
      };
    }
  }),
  send: (data) => {
    const event = {
      allow_custom_scripts: true,
      send_to: 'DC-6699907/es_dwa01/es_dw00h+standard',
      u1: window.location.href,
    };

    if (data?.marca?.nombre) {
      event.u2 = data?.marca?.nombre;
    }

    if (data?.nombre) {
      event.u3 = data?.nombre;
    }

    return (
      window.gtag('event', 'conversion', event)
    );
  },
  logPage: (pathname) => (
    window.gtag('event', 'conversion', {
      allow_custom_scripts: true,
      send_to: 'DC-6699907/es_dwa01/es_dw003+standard',
      u1: `${window.location.origin}${pathname}`,
    })
  ),
  logPageAfterTime: () => (
    window.gtag('event', 'conversion', {
      allow_custom_scripts: true,
      send_to: 'DC-6699907/es_dwa01/es_dw00j+standard',
      u1: window.location.href,
    })
  ),
};

const adobeAnalytics = {
  init: async () => new Promise((accept) => {
    const existingScript = document.getElementById('launch');
    if (!existingScript) {
      const scriptApp = document.createElement('script');
      const scriptApp2 = document.createElement('script');
      const scriptLaunch = document.createElement('script');
      scriptLaunch.src = `https:${process.env.REACT_APP_ADOBE_PIXEL}`;
      scriptApp.src = 'https://assets.adobedtm.com/extensions/EP7b1fa4581fb94dd0961a981af9997765/AppMeasurement.min.js';
      scriptApp2.src = 'https://assets.adobedtm.com/extensions/EP7b1fa4581fb94dd0961a981af9997765/AppMeasurement_Module_ActivityMap.min.js';
      scriptApp.id = 'app';
      scriptApp2.id = 'app2';
      scriptLaunch.id = 'launch';
      document.body.appendChild(scriptApp);
      document.body.appendChild(scriptApp2);
      document.body.appendChild(scriptLaunch);
      /* eslint-disable no-use-before-define */
      scriptApp.onload = () => {
        const s_account = 'vwpkwescmsprod,vwesdwaprod,vwintdwaprod,vwpkwworldprod';
        const s = window.s_gi(s_account);
        s.trackingServer = 'smetric.volkswagen.com';
        s.pageName = 'info-reserva-online';
        s.currencyCode = 'EUR';
        s.eVar1 = 'DWA-ES-ES';
        s.prop6 = 'ES';
        s.prop8 = 'D=v1';
        s.prop13 = 'vwpkwescmsprod,vwesdwaprod,vwintdwaprod,vwpkwworldprod';
        s.prop24 = '2.20.0.2';
        s.prop32 = 'DWA';
        s.prop48 = 'D=s_vi';
        s.prop61 = 'DWA:01';
        s.prop68 = `${new Date().getDate()}.${new Date().getMonth() + 1}.${new Date().getFullYear()}-${new Date().getHours()}:${new Date().getMinutes()}`;
        s.t();
        accept();
      };
    }
  }),
};

const AnalyticsService = {
  consentItems: {
    analytics: {
      id: 'analytics',
      consentGroups: [
        {
          type: 'script_load',
          members: [
            {
              priority: 100,
              source: process.env.REACT_APP_ADOBE_PIXEL,
            },
          ],
        },
        {
          type: 'tag_class',
          members: [
            'Adobe Analytics',
            'Adobe Launch',
            'Outbrain',
            'Facebook Custom Audience',
            'Livechat',
            'TradeDesk',
            'New Relic',
            'Google DoubleClick Floodlight',
            'Google Global Site Tag',
          ],
        },
      ],
    },
    comfort: {
      id: 'comfort',
      consentGroups: [
        {
          type: 'tag_class',
          members: [],
        },
      ],
    },
    personalization: {
      id: 'personalization',
      consentGroups: [
        {
          type: 'tag_class',
          members: ['Adobe Target', 'Google Dynamic Remarketing', 'VersaTag'],
        },
      ],
    },
  },

  tagList: {
    analytics: [
      'Adobe Analytics',
      'Adobe Launch',
      'Outbrain',
      'Facebook Custom Audience',
      'Livechat',
      'TradeDesk',
      'New Relic',
      'Google DoubleClick Floodlight',
      'Google Global Site Tag',
    ],
    comfort: [],
    personalization: [
      'Adobe Target',
      'Google Dynamic Remarketing',
      'VersaTag',
    ],
  },

  setPrivacyGovernor: (dataprgov) => window.localStorage.setItem('dwa_privacy_governor', JSON.stringify(dataprgov)),

  getPrivacyGovernor: () => JSON.parse(window.localStorage.getItem('dwa_privacy_governor')),

  removePrivacyGovernor: () => window.localStorage.clear('dwa_privacy_governor'),

  storeCookies: (cookiesSelected) => {
    const consentItems = cookiesSelected.map((cookieSelected) => (
      AnalyticsService.consentItems[cookieSelected]
    )) || [];

    const date = new Date();

    const n = consentItems.length ? 365 : 1;

    const dataprgov = {
      consentItems,
      date: date.toISOString(),
      validUntil: new Date(date.getTime() + 864e5 * n).toISOString(),
      version: '2021-02-22',
    };

    AnalyticsService.setPrivacyGovernor(dataprgov);

    if (window?.digitalData?.policy) {
      window.digitalData.policy.taglist = (cookiesSelected.map((cookieSelected) => (
        AnalyticsService.tagList[cookieSelected]
      ))).flat(1) || [];
    }
  },

  isValidPrivacyGovernor: () => {
    const privacyGovernor = AnalyticsService.getPrivacyGovernor();
    return privacyGovernor
      ? new Date(privacyGovernor.validUntil).getTime() - new Date().getTime() > 0
      : false;
  },

  rebuildDigitalData: () => {
    if (!window.digitalData.policy.taglist) {
      const dwaPrivacyGovernor = AnalyticsService.getPrivacyGovernor();
      window.digitalData.policy.taglist = (dwaPrivacyGovernor.consentItems.map((consentItem) => (
        AnalyticsService.tagList[consentItem.id]
      ))).flat(1) || [];
    }
  },

  initialize: async ({
    google, twitter, facebook, googleTagManager,
  }) => {
    AnalyticsService.rebuildDigitalData();
    google && googleAnalytics.init(google);
    facebook && await facebookAnalytics.init(facebook);
    googleTagManager && await googleTagManagerAnalytics.init(googleTagManager);
    twitter && twitterAnalytics.init(twitter);
    await adobeAnalytics.init();
    await floodlightAnalytics.init();
  },

  logPage: (pathname) => {
    googleAnalytics.logPage(pathname);
    facebookAnalytics.logPage(pathname);
    twitterAnalytics.logPage(pathname);
    floodlightAnalytics.logPage(pathname);
  },

  logPageAfterTime: (pathname) => {
    floodlightAnalytics.logPageAfterTime(pathname);
  },

  analyticsName: {
    GOOGLE_ANALYTICS: 'googleAnalytics',
    FLOODLIGHT_ANALYTICS: 'floodlightAnalytics',
    FACEBOOK_ANALYTICS: 'facebookAnalytics',
    OMNITURE_ANALYTICS: 'omnitureAnalytics',
    TWITTER_ANALYTICS: 'twitterAnalytics',
    GOOGLE_TAG_MANAGER_ANALYTICS: 'googleTagManagerAnalytics',
  },

  categorysName: {
    bannerPlay: {
      google: 'Ver spot',
    },
    buttonReserve: {
      google: 'Reserva online',
      facebook: 'ViewContent',
    },
    buttonReserveOnlineVW: {
      google: 'Reserva online VW',
      facebook: 'ViewContent',
    },
    buttonReserveOnlineSeat: {
      google: 'Reserva online SEAT',
      facebook: 'ViewContent',
    },
    buttonReserveOnlineSkoda: {
      google: 'Reserva online SKODA',
      facebook: 'ViewContent',
    },
    buttonReserveOnlineLcv: {
      google: 'Reserva online LCV',
      facebook: 'ViewContent',
    },
    buttonKnowMore: {
      google: 'Saber mas servicios',
      facebook: 'Search',
    },
    share: {
      google: 'Share',
    },
    scrolling: {
      google: 'Scrolling',
    },
  },

  /**
   * analyticServices: Array with type analytic service
   * [google analytic, facebook analytic, omniture, intern]
   * page: Page where set event
   * action: action type [example: click, play]
   * data: information to send
  */
  setEvent: (analyticServices, category, action, data) => {
    if (document.cookie.match('(^|;) ?_ga=([^;]*)(;|$)')) {
      for (let index = 0; index < analyticServices.length; index += 1) {
        const analyticService = analyticServices[index];
        if (analyticService === 'googleAnalytics') {
          let dataGoogle = data;
          if (typeof data === 'object') {
            dataGoogle = data.google;
          }
          googleAnalytics.send(category.google, action, dataGoogle);
        } else if (analyticService === 'floodlightAnalytics') {
          let dataFloodlight = data;
          if (typeof data === 'object') {
            dataFloodlight = data.floodlight;
          }
          floodlightAnalytics.send(dataFloodlight);
        } else if (analyticService === 'facebookAnalytics') {
          let facebookData = data;
          if (typeof data === 'object') {
            facebookData = data.facebook;
          }
          facebookAnalytics.send(category.facebook, facebookData);
        } else if (analyticService === 'googleTagManagerAnalytics') {
          let dataTagManager = data;
          if (typeof data === 'object') {
            dataTagManager = data.tagManager;
          }
          googleTagManagerAnalytics.send(dataTagManager);
        }
      }
    }
  },
};

export default AnalyticsService;
