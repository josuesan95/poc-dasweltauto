/* eslint-disable jsx-a11y/media-has-caption */
import 'aos/dist/aos.css';
import React, { useEffect, useRef } from 'react';
import { isTouchDevice } from '../../helpers/swiper';

const HomePage = () => {
  const videoRef = useRef();
  const videoRef2 = useRef();
  useEffect(() => {
    if (isTouchDevice()) {
      videoRef.current.addEventListener('touchstart', () => {
        videoRef.current.play();
      });
    } else {
      videoRef.current.addEventListener('click', () => {
        videoRef.current.play();
      });
    }
  }, [videoRef]);

  const handleVideo = () => {
    videoRef.current.classList.add('hide-video');
    videoRef2.current.play();
    videoRef2.current.classList.remove('hide-video');
  };
  return (
    <>
      <div className="relative max-h-screen">
        <video onEnded={handleVideo} ref={videoRef} className="bg-presentation" src="/assets/videos/felicidad1.mp4" playsInline />
        <video ref={videoRef2} className="bg-presentation hide-video video-abs" src="/assets/videos/felicidad2.mp4" playsInline muted />
      </div>
    </>
  );
};
export default HomePage;
